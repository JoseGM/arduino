//TMP36 Pin Variables
int temperaturePin = 0; //the analog pin the TMP36's Vout (sense) pin is connected to
                        //the resolution is 10 mV / degree centigrade 
                        //(500 mV offset) to make negative temperatures an option
int potentiometerPin = 5;
                        
int redLedPin = 9;
int greenLedPin = 13;
boolean tempOk = false;
/*
 * setup() - this function runs once when you turn your Arduino on
 * We initialize the serial connection with the computer
 */
void setup()
{
  Serial.begin(9600);  //Start the serial connection with the copmuter
                       //to view the result open the serial monitor 
                       //last button beneath the file bar (looks like a box with an antenae)
   pinMode(redLedPin, OUTPUT);
   pinMode(greenLedPin, OUTPUT);
}

void loop()                     // run over and over again
{
  float temperature = getVoltage(temperaturePin);  //getting the voltage reading from the temperature sensor
  temperature = (temperature - .5) * 100;          //converting from 10 mv per degree wit 500 mV offset
                                                  //to degrees ((volatge - 500mV) times 100)
                                                  
  float tempSwitch = getVoltage(potentiometerPin);
  tempSwitch = tempSwitch * 40 / 5;
 
  if(temperature>tempSwitch)
  {
    if(tempOk)
    {
       Serial.println("RED");
       tempOk = false;
       digitalWrite(redLedPin, HIGH);
       digitalWrite(greenLedPin, LOW);
    }
 }
  else
  {
    if(!tempOk)
    {
      Serial.println("GREEN");
      tempOk = true;
      digitalWrite(redLedPin, LOW);
      digitalWrite(greenLedPin, HIGH);
    }
  }
   
  Serial.println(temperature);
  Serial.println(tempSwitch);                     //printing the result
  delay(1000);                                     //waiting a second
}

/*
 * getVoltage() - returns the voltage on the analog input defined by
 * pin
 */
float getVoltage(int pin){
 return (analogRead(pin) * .004882814); //converting from a 0 to 1023 digital range
                                        // to 0 to 5 volts (each 1 reading equals ~ 5 millivolts
}
